package business;

import com.jayway.restassured.path.json.JsonPath;
import dao.database.FlightDAO;
import dao.domain.Flight;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.*;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.List;

public class UserServlet extends HttpServlet {
    public void init() throws ServletException {

    }

    public void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html");
        FlightDAO flightDAO = new FlightDAO();
        List<Flight> allFlights = flightDAO.findAllFlights();
        PrintWriter pw = response.getWriter();
        pw.println("<html><table border='1'>");
        pw.println("<tr><th>Flight number</th>");
        pw.println("<th>Airplane type</th>");
        pw.println("<th>Departure City</th>");
        pw.println("<th>Departure Time</th>");
        pw.println("<th>Arrival City</th>");
        pw.println("<th>Arrival Time</th></tr>");
        for (Flight flight : allFlights) {
            pw.println("<tr><td>" + flight.getFlightNo() + "</td>");
            pw.println("<td>" + flight.getAirplaneType() + "</td>");
            pw.println("<td>" + flight.getDepartureCity().getCityName() + "</td>");
            pw.println("<td>" + flight.getDepartureTime() + "</td>");
            pw.println("<td>" + flight.getArrivalCity().getCityName() + "</td>");
            pw.println("<td>" + flight.getArrivalTime() + "</td></tr>");
        }
        pw.println("</table>");

        pw.println("<form id=\"contactform\" method=\"post\">\n" +
                "    <label>Latitude</label>\n" +
                "    <input id = \"latitude\" type=\"text\" name=\"latitude\" required>\n" +
                "    <label>Longitude</label>\n" +
                "    <input id=\"longitude\" type=\"text\" name=\"longitude\" required>\n" +
                "    <button type=\"submit\">Find time</button>\n" +
                "</form></html>");
    }

    public void doPost(HttpServletRequest request, HttpServletResponse response) throws IOException {
        String latitude = request.getParameter("latitude");
        String longitude = request.getParameter("longitude");
        String apiurl = "http://api.geonames.org/timezoneJSON?lat="+latitude+"&lng="+longitude+"&username=dianabucur";
        HttpURLConnection conn=null;
        StringBuilder result = new StringBuilder();
        try {
            URL url = new URL(apiurl);
            conn = (HttpURLConnection) url.openConnection();
            InputStream in = new BufferedInputStream(conn.getInputStream());
            BufferedReader reader = new BufferedReader(new InputStreamReader(in));
            String line;
            while ((line = reader.readLine()) != null) {
                result.append(line);
            }

        }catch( Exception e) {
            e.printStackTrace();
        }
        finally {
            conn.disconnect();
        }
        String time = JsonPath.from(result.toString()).get("time");
        response.setContentType("text/html");
        FlightDAO flightDAO = new FlightDAO();
        List<Flight> allFlights = flightDAO.findAllFlights();
        PrintWriter pw = response.getWriter();
        pw.println("<html><table border='1'>");
        pw.println("<tr><th>Flight number</th>");
        pw.println("<th>Airplane type</th>");
        pw.println("<th>Departure City</th>");
        pw.println("<th>Departure Time</th>");
        pw.println("<th>Arrival City</th>");
        pw.println("<th>Arrival Time</th></tr>");
        for (Flight flight : allFlights) {
            pw.println("<tr><td>" + flight.getFlightNo() + "</td>");
            pw.println("<td>" + flight.getAirplaneType() + "</td>");
            pw.println("<td>" + flight.getDepartureCity().getCityName() + "</td>");
            pw.println("<td>" + flight.getDepartureTime() + "</td>");
            pw.println("<td>" + flight.getArrivalCity().getCityName() + "</td>");
            pw.println("<td>" + flight.getArrivalTime() + "</td></tr>");
        }
        pw.println("</table>");

        pw.println("<form id=\"contactform\" method=\"post\">\n" +
                "    <label>Latitude</label>\n" +
                "    <input id = \"latitude\" type=\"text\" name=\"latitude\" required>\n" +
                "    <label>Longitude</label>\n" +
                "    <input id=\"longitude\" type=\"text\" name=\"longitude\" required>\n" +
                "    <button type=\"submit\">Find time</button>\n" +
                "</form>");
        pw.println("<p>"+time+"</p></html>");
    }
}
