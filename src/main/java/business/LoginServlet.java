package business;

import dao.database.UserDAO;
import dao.domain.User;
import dao.domain.UserRole;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;

public class LoginServlet extends HttpServlet {

    public void init() throws ServletException {
    }

    public void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

        // Set response content type
        response.setContentType("text/html");

        RequestDispatcher view = request.getRequestDispatcher("WEB-INF/login.html");
        view.include(request, response);
    }

    public void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        String username = request.getParameter("username");
        String password = request.getParameter("password");
        UserDAO userDAO = new UserDAO();
        User user = userDAO.findUser(username, password);
        HttpSession snsobj=request.getSession(true);
        if (user.getUserRole() != null)
            if (user.getUserRole().equals(UserRole.ADMIN)) {
                snsobj.setAttribute("role", "admin");
                response.sendRedirect("/flights");
            } else {
                snsobj.setAttribute("role", "user");
                response.sendRedirect("/allflights");
            }
    }


    public void destroy() {
        // do nothing.
    }
}
