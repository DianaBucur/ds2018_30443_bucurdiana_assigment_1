package business;

import dao.database.CityDAO;
import dao.database.FlightDAO;
import dao.domain.City;
import dao.domain.Flight;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.io.PrintWriter;
import java.sql.Timestamp;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

public class FlightServlet extends HttpServlet {

    public void init() throws ServletException {
    }

    public void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        HttpSession snsobj=request.getSession(true);
        String role= (String) snsobj.getAttribute("role");
        PrintWriter printwriter=response.getWriter();
        response.setContentType("text/html");
        if(role.equals("admin")) {
            response.setContentType("text/html");
            RequestDispatcher view = request.getRequestDispatcher("WEB-INF/viewflights.html");
            view.include(request, response);

            response.setContentType("text/html");
            FlightDAO flightDAO = new FlightDAO();
            List<Flight> allFlights = flightDAO.findAllFlights();
            PrintWriter pw = response.getWriter();
            pw.println("<html><table border='1'>");
            pw.println("<tr><th>Flight number</th>");
            pw.println("<th>Airplane type</th>");
            pw.println("<th>Departure City</th>");
            pw.println("<th>Departure Time</th>");
            pw.println("<th>Arrival City</th>");
            pw.println("<th>Arrival Time</th></tr>");
            for (Flight flight : allFlights) {
                pw.println("<tr><td>" + flight.getFlightNo() + "</td>");
                pw.println("<td>" + flight.getAirplaneType() + "</td>");
                pw.println("<td>" + flight.getDepartureCity().getCityName() + "</td>");
                pw.println("<td>" + flight.getDepartureTime() + "</td>");
                pw.println("<td>" + flight.getArrivalCity().getCityName() + "</td>");
                pw.println("<td>" + flight.getArrivalTime() + "</td></tr>");
            }
            pw.println("</table></html>");
        }
        else{
            printwriter.println("Error! You do not have access on this page.");
        }
    }

    public void doPost(HttpServletRequest request, HttpServletResponse response) throws IOException {
        if (request.getParameter("button").equals("add")) {
            Long flightNr = Long.parseLong(request.getParameter("flightnumber"));
            String airplaneType = request.getParameter("airplanetype");
            String departureCity = request.getParameter("depcity");
            String departureTime = request.getParameter("deptime");
            String arrivalCity = request.getParameter("arrcity");
            String arrivalTime = request.getParameter("arrtime");
            CityDAO cityDAO = new CityDAO();
            City depaCity = cityDAO.findCity(departureCity);
            City arrCity = cityDAO.findCity(arrivalCity);

            SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
            Date parsedDate = null;
            Date parseDate2 = null;
            try {
                parsedDate = dateFormat.parse(departureTime);
                parseDate2 = dateFormat.parse(arrivalTime);
            } catch (ParseException e) {
                e.printStackTrace();
            }
            Timestamp timestamp1 = new java.sql.Timestamp(parsedDate.getTime());
            Timestamp timestamp2 = new java.sql.Timestamp(parseDate2.getTime());

            Flight flight = new Flight();
            flight.setFlightNo(flightNr);
            flight.setAirplaneType(airplaneType);
            flight.setDepartureCity(depaCity);
            flight.setArrivalCity(arrCity);
            flight.setDepartureTime(timestamp1);
            flight.setArrivalTime(timestamp2);
            FlightDAO flightDAO = new FlightDAO();
            flightDAO.addFlight(flight);
            response.sendRedirect("/flights");
        } else if (request.getParameter("button").equals("delete")) {
            int flightNr = Integer.parseInt(request.getParameter("flightnumber"));
            FlightDAO flightDAO = new FlightDAO();
            flightDAO.deleteFlight(flightNr);
            response.sendRedirect("/flights");
        } else if (request.getParameter("button").equals("update")) {
            Long flightNr = Long.parseLong(request.getParameter("flightnumber"));
            String airplaneType = request.getParameter("airplanetype");
            String departureCity = request.getParameter("depcity");
            String departureTime = request.getParameter("deptime");
            String arrivalCity = request.getParameter("arrcity");
            String arrivalTime = request.getParameter("arrtime");
            CityDAO cityDAO = new CityDAO();
            Flight flight = new Flight();
            if (departureCity != "") {
                City depaCity = cityDAO.findCity(departureCity);
                flight.setDepartureCity(depaCity);
            }
            if (arrivalCity != "") {
                City arrCity = cityDAO.findCity(arrivalCity);
                flight.setArrivalCity(arrCity);
            }
            flight.setFlightNo(flightNr);
            flight.setAirplaneType(airplaneType);
            SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
            Date parsedDate = null;
            Date parseDate2 = null;
            try {
                if (departureTime != "") {
                    parsedDate = dateFormat.parse(departureTime);
                    Timestamp timestamp1 = new java.sql.Timestamp(parsedDate.getTime());
                    flight.setDepartureTime(timestamp1);
                }
                if (arrivalTime != "") {
                    parseDate2 = dateFormat.parse(arrivalTime);
                    Timestamp timestamp2 = new java.sql.Timestamp(parseDate2.getTime());
                    flight.setArrivalTime(timestamp2);
                }
            } catch (ParseException e) {
                e.printStackTrace();
            }
            FlightDAO flightDAO = new FlightDAO();
            flightDAO.updateFlight(flight);
            response.sendRedirect("/flights");
        }
    }

    public void destroy() {
        // do nothing.
    }
}
