package dao.database;

import dao.domain.Flight;
import org.hibernate.*;
import org.hibernate.cfg.Configuration;

import java.util.List;

public class FlightDAO {

    private SessionFactory factory;

    public FlightDAO() {
        this.factory = new Configuration().configure("hibernate.cfg.xml").buildSessionFactory();
    }

    public Flight addFlight(Flight flight) {
        Long flightId = -1l;
        Session session = factory.openSession();
        Transaction tx = null;
        try {
            tx = session.beginTransaction();
            flightId = (Long) session.save(flight);
            flight.setId(flightId);
            tx.commit();
        } catch (HibernateException e) {
            if (tx != null) {
                tx.rollback();
            }
        } finally {
            session.close();
        }
        return flight;
    }

    public void deleteFlight(int flightNr) {
        Session session = factory.openSession();
        Transaction tx = null;
        try {
            tx = session.beginTransaction();
            Query query = session.createQuery("DELETE FROM Flight WHERE flightNo= :FlightParam");
            query.setInteger("FlightParam", flightNr);
            query.executeUpdate();
            tx.commit();
        } catch (HibernateException e) {
            if (tx != null) {
                tx.rollback();
            }
        } finally {
            session.close();
        }
    }

    public Flight findFlight(Long flightNr) {
        Session session = factory.openSession();
        Transaction tx = null;
        Flight flight = null;
        try {
            tx = session.beginTransaction();
            Query query = session.createQuery("from Flight where flightNo= :FlightParam");
            query.setLong("FlightParam", flightNr);
            flight = (Flight) query.list().get(0);
            tx.commit();
        } catch (HibernateException e) {
            if (tx != null) {
                tx.rollback();
            }
        } finally {
            session.close();
        }
        return flight;
    }

    public void updateFlight(Flight newFlight) {
        Session session = factory.openSession();
        Transaction tx = null;
        try {
            tx = session.beginTransaction();
            Flight persistedFlight = findFlight(newFlight.getFlightNo());
            if (newFlight.getAirplaneType() != null)
                persistedFlight.setAirplaneType(newFlight.getAirplaneType());
            if (newFlight.getDepartureCity() != null)
                persistedFlight.setDepartureCity(newFlight.getDepartureCity());
            if (newFlight.getDepartureTime() != null)
                persistedFlight.setDepartureTime(newFlight.getDepartureTime());
            if (newFlight.getArrivalCity() != null)
                persistedFlight.setArrivalCity(newFlight.getArrivalCity());
            if (newFlight.getArrivalTime() != null)
                persistedFlight.setArrivalTime(newFlight.getArrivalTime());
            session.update(persistedFlight);
            tx.commit();
        } catch (HibernateException e) {
            if (tx != null) {
                tx.rollback();
            }
        } finally {
            session.close();
        }
    }

    @SuppressWarnings("unchecked")
    public List<Flight> findAllFlights() {
        Session session = factory.openSession();
        Transaction tx = null;
        List<Flight> flights = null;
        try {
            tx = session.beginTransaction();
            flights = session.createQuery("FROM Flight").list();
            tx.commit();
        } catch (HibernateException e) {
            if (tx != null) {
                tx.rollback();
            }
        } finally {
            session.close();
        }
        return flights;
    }
}
