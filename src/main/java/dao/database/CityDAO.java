package dao.database;

import dao.domain.City;
import org.hibernate.*;
import org.hibernate.cfg.Configuration;

public class CityDAO {
    private SessionFactory factory;

    public CityDAO() {
        this.factory = new Configuration().configure("hibernate.cfg.xml").buildSessionFactory();
    }

    @SuppressWarnings("unchecked")
    public City findCity(String name) {
        Session session = factory.openSession();
        Transaction tx = null;
        City city = null;
        try {
            tx = session.beginTransaction();
            Query query = session.createQuery("from City where name= :CityParam");
            query.setString("CityParam", name);
            city = (City) query.list().get(0);
            tx.commit();
        } catch (HibernateException e) {
            if (tx != null) {
                tx.rollback();
            }
        }
        return city;
    }
}
