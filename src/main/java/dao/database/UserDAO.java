package dao.database;

import dao.domain.User;
import org.hibernate.*;
import org.hibernate.cfg.Configuration;

public class UserDAO {

    private SessionFactory factory;

    public UserDAO() {
        this.factory = new Configuration().configure("hibernate.cfg.xml").buildSessionFactory();
    }

    @SuppressWarnings("unchecked")
    public User findUser(String username, String password) {
        Session session = factory.openSession();
        Transaction tx = null;
        User user = null;
        try {
            tx = session.beginTransaction();
            Query query = session.createQuery("from User where username= :UsernameParam and password= :PassParameter");
            query.setString("UsernameParam", username);
            query.setString("PassParameter", password);
            user = (User) query.list().get(0);
            tx.commit();
        } catch (HibernateException e) {
            if (tx != null) {
                tx.rollback();
            }
        } finally {
            session.close();
        }
        return user;
    }
}
